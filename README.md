# Databases

### MySQL

#### General

 - launching MSQL: `sudo service mysql start` or `sudo /etc/init.d/mysql start`
 - launching command line client `mysql -u [username] -p [databasename]`
 - import database schema: `mysql -u [username] -p [databasename] < [schema.sql]` 
 - show all databases `show databases;` 
 - check which database is selected: `select database();`
 - select database: `use [database];`
 - show all tables: `show tables;`
 - show table columns: `describe [table_name];`
 
 - delete data from table: `DELETE from [table_name]`
 
 #### Admin
 
 - accessing admin stuff: `mysqladmin -u [username]` 
 - create database: `create database [databasename];`
 - delete database:`drop database [databasename];` Carefull, this cannot be undone!
 - create a database schema (dump without data). Exit mysql console first, then: `mysqldump -d -h localhost -u root -p[password] [database] > [filename.sql]` . _-d_ option removes the data from the dump.  
 - import schema into database (exit mysql console first): `mysql -hlocalhost -uroot -p [database] < [filename.sql]`
 
 

#### Troubleshooting
___

If you get `ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)` after instalation, do:
  - `sudo /etc/init.d/mysql stop`
  - `sudo mysqld_safe --skip-grant-tables &`
  - for the above, if you get an error `mysqld_safe Directory '/var/run/mysqld' for UNIX socket file don't exists`, do:
  1. `sudo mkdir -p /var/run/mysqld`
  2. `sudo chown mysql:mysql /var/run/mysqld`
  - then `sudo mysqld_safe --skip-grant-tables &` again
  - in a new terminal, do: 
  - `mysql -uroot`
  - `use mysql;`
  - `update user set authentication_string=password('XXXX') where user='root';` or `SET PASSWORD FOR 'root' = PASSWORD('XXXX');`

  ___

If you get `Job for mysql.service failed because the control process exited with error code. See "systemctl status mysql.service" and "journalctl -xe" for details` and then `Process: 4331 ExecStartPre=/usr/share/mysql/mysql-systemd-start pre (code=exited, status=1/FAILURE)` what seems to help is:
- `sudo rm -R /var/lib/mysql/`
- `sudo mkdir /var/lib/mysql`
- `sudo chown mysql /var/lib/mysql`
- `sudo chgrp mysql /var/lib/mysql`
- `sudo mysqld --initialize`

To update mysql run:
`sudo apt install -f`
  
